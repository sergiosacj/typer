#!/usr/bin/make -f
export DH_VERBOSE=1

# Typer versions greater than 0.5.0 run tests that are influenced by the
# python3-rich dependency. This library respects some standard environment
# variables such as COLUMNS, but this variable can break tests that perform
# string comparisons if the terminal width is small. Upstream handles this by
# running tests with a large terminal width, since typer can override the
# terminal width by setting the variable below.
export TERMINAL_WIDTH=3000
# Force disable terminal for tests inside of pytest. See scripts/test.sh.
# When the variable below is enabled, rich console is set to not detect the
# terminal. If Rich detects that it is not writing to a terminal it will
# strip control codes from the output.
export _TYPER_FORCE_DISABLE_TERMINAL=1
# The reason why tests related to shell completion fails is that these tests
# need us to be running under a supported shell, that is, bash rather than sh.
# Shell detection with shellingham is so thorough it is not possible to fool
# it by any combination of:
# - export SHELL=/bin/bash
# - bash -c 'pytest'
TESTS_TO_EXCLUDE='not test_show_completion and not test_install_completion'
# These cannot find the typer package because the tests override PYTHONPATH.
TESTS_TO_IGNORE=--ignore=tests/test_tutorial/test_subcommands/test_tutorial001.py \
--ignore=tests/test_tutorial/test_subcommands/test_tutorial003.py
export PYBUILD_NAME=typer

%:
	dh $@ --with python3 --buildsystem=pybuild

execute_after_dh_auto_build:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	mkdocs build --site-dir $(CURDIR)/html --config-file $(CURDIR)/mkdocs.yml
	rm -f $(CURDIR)/html/404.html $(CURDIR)/html/sitemap.xml.gz
endif

override_dh_auto_test:
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="pytest-3 -k ${TESTS_TO_EXCLUDE} ${TESTS_TO_IGNORE}" \
	dh_auto_test
endif

execute_after_dh_installdocs:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	dh_mkdocs -Tmkdocs -Treadthedocs -Tlibjs-underscore -Tnode-jquery
endif

execute_after_dh_install:
	mkdir -p debian/typer/usr/bin
	mv debian/python3-typer/usr/bin/typer debian/typer/usr/bin/typer
